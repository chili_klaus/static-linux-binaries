#!/usr/bin/env sh

# Check if exactly 2 parameters are passed
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <program> <arch>"
    exit 1
fi

PROGRAM=$1
ARCH=$2

DOCKER_BUILDKIT=1

# Run the Docker build command with the specified parameters
docker build \
       --file "$PROGRAM/Dockerfile" \
       --platform="linux/$ARCH" \
       --output "target/$ARCH/" .
